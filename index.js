const path = require('path');
const { Telegraf } = require('telegraf');

require('dotenv').config({ path: path.join(__dirname, '.env') });
const { cronMasuk, cronKeluar, listenStatus } = require('./cronJob');

const bot = new Telegraf(process.env.BOT_TOKEN);

function runner() {
  cronMasuk.start();
  cronKeluar.start();
}
function stopper() {
  cronMasuk.stop();
  cronKeluar.stop();
}

bot.start(async (ctx) => {
  listenStatus.emit('statusChange', true);
  await ctx.reply('Absen WII otomatis jalan man!');
  await bot.telegram.sendMessage(ctx.chat.id, 'Otomatis Status : ON', {
    reply_markup: {
      keyboard: [
        [
          {
            text: 'STOP',
          },
        ],
      ],
    },
  });
  listenStatus.on('hasilAbsenBot', async (message) => {
    await bot.telegram.sendMessage(ctx.chat.id, message, {
      reply_markup: {
        keyboard: [
          [
            {
              text: 'STOP',
            },
          ],
        ],
      },
    });
  });
});
bot.hears('STOP', async (ctx) => {
  listenStatus.emit('statusChange', false);

  await bot.telegram.sendMessage(ctx.chat.id, 'Otomatis Status : OFF', {
    reply_markup: {
      keyboard: [
        [
          {
            text: 'RUN',
          },
        ],
      ],
    },
  });
});
bot.hears('RUN', async (ctx) => {
  listenStatus.emit('statusChange', true);
  await bot.telegram.sendMessage(ctx.chat.id, 'Otomatis Status : ON', {
    reply_markup: {
      keyboard: [
        [
          {
            text: 'STOP',
          },
        ],
      ],
    },
  });
  listenStatus.on('hasilAbsenBot', async (message) => {
    await bot.telegram.sendMessage(ctx.chat.id, message, {
      reply_markup: {
        keyboard: [
          [
            {
              text: 'STOP',
            },
          ],
        ],
      },
    });
  });
});
listenStatus.on('statusChange', (active) => {
  if (active) {
    runner();
    console.log('Absen Otomatis: On');
  } else {
    stopper();
    console.log('Absen Otomatis: Off');
  }
});
bot.launch();
