const axios = require('axios');
const FormData = require('form-data');
const fs = require('fs');
const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });
const baseUrl = process.env.BASE_URL_WS;

async function captcha(session_id) {
  const { data } = await axios.get(`${baseUrl}/auth/captcha`, {
    headers: { Authorization: `Bearer ${session_id}` },
    validateStatus(status) {
      return true; // status >= 200 && status < 300; // default
    },
  });
  return data.data.captcha;
}
exports.absenMasuk = async (session_id, sendData) => {
  const { data } = await axios.post(
    `${baseUrl}/v1/attendance/clockin`,
    sendData,
    {
      headers: Object.assign(
        {
          Authorization: `Bearer ${session_id}`,
        },
        sendData.getHeaders()
      ),
      validateStatus(status) {
        return true; // status >= 200 && status < 300; // default
      },
    }
  );
  if (data.success === true) {
    console.log(`${data.data.date} ${data.data.time} : absen masuk berhasil`);
    return `${data.data.date} ${data.data.time} : absen masuk berhasil`;
  } else {
    console.log('Absen masuk gagal');
    return 'Absen masuk gagal';
  }
};
exports.absenKeluar = async (session_id, sendData) => {
  const { data } = await axios.post(
    `${baseUrl}/v1/attendance/clockout`,
    sendData,
    {
      headers: Object.assign(
        {
          Authorization: `Bearer ${session_id}`,
        },
        sendData.getHeaders()
      ),
      validateStatus(status) {
        return true; // status >= 200 && status < 300; // default
      },
    }
  );

  if (data.success === true) {
    console.log(`${data.data.date} ${data.data.time} : absen keluar berhasil`);
    return `${data.data.date} ${data.data.time} : absen keluar berhasil`;
  } else {
    console.log('Absen keluar gagal');
    return 'Absen keluar gagal';
  }
};
exports.sendData = async () => {
  const dataWiski = new FormData();
  const img = fs.createReadStream(path.join(__dirname, '/fotoku.jpg'));
  img.on('error', function (err) {
    console.log(err);
  });
  dataWiski.append('captcha', await captcha(process.env.SESSION_ID));
  dataWiski.append('latitude', -7.734635);
  dataWiski.append('longitude', 110.377921);
  dataWiski.append('faceFile', img);
  return dataWiski;
};
