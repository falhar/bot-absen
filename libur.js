const axios = require('axios');
const cheerio = require('cheerio');
const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });

async function tahun() {
  try {
    const { data } = await axios.get(process.env.BASE_URL);
    const cheridata = cheerio.load(data);
    const source = cheridata('.dropdown-menu').eq(0).children().children();
    const years = [];
    source.each(async (i, dt) => {
      const uncleanyear = dt.children[0].data.split(' ');
      const year = parseInt(uncleanyear[uncleanyear.length - 1]);
      if (year === new Date().getFullYear()) {
        years.push({
          year,
          link: dt.attribs.href,
        });
      }
    });
    return years[0];
  } catch (error) {
    console.error(error.message, error);
  }
}

module.exports = async function libur() {
  try {
    const th = await tahun();
    const libur = {};
    const { data } = await axios.get(`${process.env.BASE_URL}${th.link}`);
    const cheridata = cheerio.load(data);
    cheridata('.libnas-calendar-holiday-title').each(async (i, dt) => {
      const day = dt.children[1].children[0].data;
      const date = dt.children[2].children[0].data;
      const event = dt.children[0].children[0].children[0].children[0].data;
      const arrDate = date.split(' ');

      const tgl = arrDate[0];
      const tgl2 = arrDate[2];
      const bln = arrDate[arrDate.length - 2];
      const bulan = [
        'januari',
        'februari',
        'maret',
        'april',
        'mei',
        'juni',
        'juli',
        'agustus',
        'september',
        'oktober',
        'desember',
      ];
      const harini = new Date();
      if (
        `${tgl}-${bulan.findIndex((v) => v === bln.toLowerCase())}` ===
          `${harini.getDate()}-${harini.getMonth()}` ||
        `${tgl2}-${bulan.findIndex((v) => v === bln.toLowerCase())}` ===
          `${harini.getDate()}-${harini.getMonth()}`
      ) {
        libur.day = day;
        libur.date = date;
        libur.event = event;
      }
    });

    return libur;
  } catch (error) {
    console.error(error.message, error);
  }
};
