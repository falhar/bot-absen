const Job = require('cron').CronJob;
const EventEmitter = require('events');
const path = require('path');

require('dotenv').config({ path: path.join(__dirname, '.env') });
const { absenMasuk, absenKeluar, sendData } = require('./job');
const libur = require('./libur');

class ListenStatus extends EventEmitter {}

const listenStatus = new ListenStatus();
exports.listenStatus = listenStatus;

exports.cronMasuk = new Job(
  '0 57 7 * * *',
  async () => {
    const date = await libur();
    if (!date?.day && new Date().getDay() !== 0 && new Date().getDay() !== 6) {
      const absenDatang = await absenMasuk(
        process.env.SESSION_ID,
        await sendData()
      );
      listenStatus.emit('hasilAbsenBot', absenDatang);
    } else {
      listenStatus.emit(
        'hasilAbsenBot',
        `Libur ${
          new Date().getDay() === 0 || new Date().getDay() === 6
            ? 'Weekend'
            : ''
        } ${date.event ?? ''}`
      );
      console.log(
        `Libur ${
          new Date().getDay() === 0 || new Date().getDay() === 6
            ? 'Weekend'
            : ''
        } ${date.event ?? ''}`
      );
    }
  },
  null,
  true,
  'Asia/Jakarta'
);

exports.cronKeluar = new Job(
  '0 57 17 * * *',
  async () => {
    const date = await libur();
    if (!date?.day && new Date().getDay() !== 0 && new Date().getDay() !== 6) {
      const absenPulang = await absenKeluar(
        process.env.SESSION_ID,
        await sendData()
      );
      listenStatus.emit('hasilAbsenBot', absenPulang);
    } else {
      console.log(
        `Libur ${
          new Date().getDay() === 0 || new Date().getDay() === 6
            ? 'Weekend'
            : ''
        } ${date.event ?? ''}`
      );
    }
  },
  null,
  true,
  'Asia/Jakarta'
);
